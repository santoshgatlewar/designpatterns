## Design Patterns 

This project is to understand some of the design patterns with examples and could also be used as a quick reference.

## Motivation

A software developer with good understanding of design patterns could provide a better solution for a given problem.

I have always belived in the above line and that is what the main motivation behing creating this project.

## Installation

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)